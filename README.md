# UMKM Connect

## 🚀 Quick start

To run this locally:

1. Clone project `git clone https://gitlab.com/anggifergian/umkmcenter.git`
2. Install all dependencies using `yarn install` or `npm install`
3. Start the development server using `yarn run dev` or `npm run dev`
4. Open up [http://localhost:3000](http://localhost:3000)
5. Happy Hacking!

### Production build

You can generate an optimized distribution bundle. To do this run the command:

```CLI
yarn run build
```

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

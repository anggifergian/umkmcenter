import { Col, Container, Row } from "react-bootstrap";
import styles from "../../styles/Footer.module.scss";
import Link from "next/link";

function Footer() {
  return (
    <footer className={`${styles.footer} ${styles.bgDark}`}>
      <Container>
        <Row
          className={`${styles.format} py-3`}
          style={{ marginRight: 0, marginLeft: 0 }}
        >
          <Col xs="12" md="6">
            <div className={styles.company}>
              <div className={styles.logo}>
                <img src="/logo-primary.png" />
              </div>
              <div className={styles.biodata}>
                <h5>UMKM Connect</h5>
                <div className={styles.contact}>
                  Centennial Tower, 29 Floor <br />
                  Jl Jend Gatot Subroto no 24-25, <br />
                  Jakarta Selatan, Jakarta 12950
                </div>
              </div>
            </div>
          </Col>
          <Col xs="12" md="3">
            <div className={styles.navigasi}>
              <ul className={styles.list}>
                <li>
                  <Link href="/about-us">Tentang Kami</Link>
                </li>
                <li>
                  <Link href="/terms">Syarat Ketentuan</Link>
                </li>
                <li>
                  <Link href="/contact-us">Kontak Kami</Link>
                </li>
              </ul>
            </div>
          </Col>
          <Col xs="12" md="3">
            <div>
              <h6>Follow Us</h6>
            </div>
          </Col>
        </Row>
        <div className={`${styles.copyRight}`}>
          <Col>
            <div className="text-center">
              <small>© Copyright 2021 UMKM Center. All Rights Reserved</small>
            </div>
          </Col>
        </div>
      </Container>
    </footer>
  );
}

export default Footer;

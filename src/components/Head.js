import Head from "next/head";

export default function HeadComponent({ title = "UMKM Center" }) {
  return (
    <Head>
      <title>{title}</title>
      <link rel="icon" href="/logo-primary.ico" />
    </Head>
  );
}

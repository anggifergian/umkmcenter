import { Navbar, Nav, Container } from "react-bootstrap";
import Link from "next/link";
import styles from "../../styles/Navbar.module.scss";

export default function NavbarComponent() {
  return (
    <Navbar
      expand="lg"
      collapseOnSelect={true}
      sticky="top"
      className={`${styles.navbar} ${styles.shadowv18}`}
      style={{ position: "fixed", width: "100%" }}
    >
      <Container>
        <Navbar.Brand
          href="/"
          className="d-flex align-items-center"
          style={{ minHeight: 50 }}
        >
          <img src="/logo-primary.png" style={{ maxWidth: 50 }} />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav>
            <Link href="/">
              <a className="nav-link">Home</a>
            </Link>
            <Link href="/event">
              <a className="nav-link">Event</a>
            </Link>
            <Link href="/about-us">
              <a className="nav-link">Tentang Kami</a>
            </Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

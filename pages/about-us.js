// Components
import Head from "../src/components/Head";
import Navbar from "../src/components/Navbar";
import Footer from "../src/components/Footer";
import { useRouter } from "next/router";

// Styles
import globalStyle from "../styles/Global.module.scss";
import aboutStyle from "../styles/AboutUs.module.scss";
import { Container } from "react-bootstrap";

const AboutUs = () => {
  const router = useRouter();
  console.log(router.query);

  return (
    <div>
      <Head />
      <Navbar />
      <main className={globalStyle.mainSection}>
        <Container className={aboutStyle.containerAbout}>
          <h1>About Us</h1>
        </Container>
      </main>
      <Footer />
    </div>
  );
};

export default AboutUs;

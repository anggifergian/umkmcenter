import Head from "../src/components/Head";
import Navbar from "../src/components/Navbar";
import Footer from "../src/components/Footer";
import { Container } from "react-bootstrap";

const Terms = () => {
  return (
    <div>
      <Head />
      <Navbar />
      <main style={{ minHeight: 500 }}>
        <Container>
          <h1>Terms</h1>
        </Container>
      </main>
      <Footer />
    </div>
  );
};

export default Terms;

// Components
import Head from "../src/components/Head";
import Navbar from "../src/components/Navbar";
import Footer from "../src/components/Footer";
import Carousel from "../src/components/Carousel";

// Styles
import globalStyle from "../styles/Global.module.scss";
import homeStyle from "../styles/Home.module.scss";

export default function Home() {
  return (
    <div>
      <Head />
      <Navbar />

      <div className={globalStyle.mainSection}>
        <Carousel />

        <section className={homeStyle.section}>
          <div className="container">
            <main className={`row ${homeStyle.main}`}>
              <div className="col-12 col-md-10 col-lg-8 text-center">
                <h4 className={homeStyle.title}>
                  Fair, simple pricing for all.
                </h4>
                <p className={`${homeStyle.subtitle} ${homeStyle.muted10}`}>
                  All types of businesses need access to development resources,
                  so we give you the option to decide how much you need to use.
                </p>
              </div>
            </main>
            <div className={`row ${homeStyle.feature}`}>
              <div className="col-12 col-md-4">
                <div className="px-lg-4 pb-2">
                  <div className={homeStyle.icon}></div>
                  <div className={`${homeStyle.featureTitle}`}>
                    Gandiwa Smartmedia
                  </div>
                  <p className={`${homeStyle.featureBody}`}>
                    Layar TV di ruang publik menjadi media promosi yang efektif
                    dan mampu menjangkau market yang lebih luas.
                  </p>
                </div>
              </div>
              <div className="col-12 col-md-4">
                <div className="px-lg-4 pb-2">
                  <div className={homeStyle.icon}></div>
                  <div className={`${homeStyle.featureTitle}`}>
                    Gandiwa Smartwifi
                  </div>
                  <p className={`${homeStyle.featureBody}`}>
                    Wifi gratis yang tersedia dengan kecepatan tinggi menjami
                    kenyamanan pengunjung selama berada di area publik.
                  </p>
                </div>
              </div>
              <div className="col-12 col-md-4">
                <div className="px-lg-4 pb-2">
                  <div className={homeStyle.icon}></div>
                  <div className={`${homeStyle.featureTitle}`}>
                    Gandiwa Dashboard
                  </div>
                  <p className={`${homeStyle.featureBody}`}>
                    Dashboard akan menampilkan laporan kegiatan promosi bisnis
                    Anda secara realtime.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section
          style={{ backgroundColor: "#FAFBFE !important" }}
          className={homeStyle.section}
        >
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-6 order-md-2">
                <div className={homeStyle.paddingRight}>
                  <h3>Change the way you build wesites. Forever.</h3>
                  <p style={{ color: "#869ab8" }}>
                    With Purpose you get components and examples, including tons
                    of variables that will help you customize this theme with
                    ease.
                  </p>
                </div>
              </div>
              <div className="col-12 col-md-6 order-md-1">
                <div style={{ height: 200, backgroundColor: "#869ab8" }}>
                  Photo
                </div>
              </div>
            </div>
          </div>
        </section>

        <Footer />
      </div>
    </div>
  );
}
